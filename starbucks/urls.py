from django.urls import path

from . import views

app_name = 'starbucks'

urlpatterns = [
    path('starbucks/', views.index, name='index'),
]